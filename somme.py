""" Fonction Somme """


def somme(a: float, b: float) -> float:
    """
    Addition des 2 paramètres
    :param a: 'float'
    :param b: 'float'
    :return: 'float'
    """
    return a + b
